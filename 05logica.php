<html>
<head>
<title>Exemplo de PHP</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF8">
</head>
<body>
<?php
    $a = 8;
    $b = 3;
    $c = 3;
    echo "<p>a = 8</p>";
    echo "<p>b = 3</p>";
    echo "<p>c = 3</p>";
    echo "<p>'a' é igual a 'b' e 'c' é maior que 'b' ? ---- ";
    
    // ¿Que imprime? ¿Que tipo de dato es?
    echo ($a == $b) && ($c > $b),"</p>";
    echo "<p>'a' é igual a 'b' ou 'b' é igual a 'c' ? ---- ";
    
    // ¿Que imprime? ¿Que tipo de dato es?
    echo ($a == $b) || ($b == $c),"</p>";
    echo "<p>'b' non é menor ou igual a 'c' ? ---- ";
    
    // ¿Que imprime? ¿Que tipo de dato es?
    echo !($b <= $c),"</p>";
    ?>
</body> 
</html>